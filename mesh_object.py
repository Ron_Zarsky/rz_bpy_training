"""
Get the Base.blend file and training task description from:
https://datagen.atlassian.net/wiki/spaces/IT/pages/252575772/Blender+Task

Implement a function that takes a mesh object as input and returns a dictionary
of vertex coordinates in 3d, where the keys are the ids of the vertices and the values
are the 3d coordinates of the vertices.
Make sure that when you move the bones of the mesh the dictionary
of vertices gets updated accordingly.
"""

import bpy
from functools import partial
from mathutils import Vector, Matrix
from bpy.types import MeshVertex, Object, bpy_prop_collection

class TransformedVertices(object):
    @classmethod
    def transformed_vertex(cls, transform_matrix : Matrix,
                           vertex_coordinates : Vector) -> Vector:
        return transform_matrix @ vertex_coordinates

    @classmethod
    def transformed_vertex_record(cls, transform_matrix : Matrix,
                                  vertex : bpy.types.MeshVertex) -> tuple:
        return vertex.index, cls.transformed_vertex(transform_matrix, vertex.co)

    def __init__(self, vertices : bpy_prop_collection):
        self.vertices = vertices

    def __call__(self, transform_matrix : Matrix) -> dict:
        transformed_vertices = map(partial(self.transformed_vertex_record, transform_matrix),
                                   self.vertices)
        return dict(transformed_vertices)

class MeshObject(object):
    @staticmethod
    def _trigger_matrix_world_refresh() -> None:
        """object relocation by itself does NOT trigger an update of the bpy Object's matrix_world ! this line does"""
        bpy.context.view_layer.update()

    def __init__(self, ob : Object):
        self.ob = ob
        self.transformed_vertices = TransformedVertices(ob.data.vertices)

    def vertices_global_coordinates(self) -> dict:
        self._trigger_matrix_world_refresh()
        return self.transformed_vertices(self.ob.matrix_world)
