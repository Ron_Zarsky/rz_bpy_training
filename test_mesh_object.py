"""
Get the Base.blend file and training task description from:
https://datagen.atlassian.net/wiki/spaces/IT/pages/252575772/Blender+Task

Implement a function that takes a mesh object as input and returns a dictionary
of vertex coordinates in 3d, where the keys are the ids of the vertices and the values
are the 3d coordinates of the vertices.
Make sure that when you move the bones of the mesh the dictionary
of vertices gets updated accordingly.
"""

import sys
import unittest
import bpy
from mathutils import Vector
from mesh_object import MeshObject

""" added these lines to enable breakpoints in pycharm --> the order is important !!!"""
sys.path.append("/Applications/PyCharm.app/Contents/debug-eggs/pydevd-pycharm.egg")
import pydevd
pydevd.settrace("localhost", port=1090, stdoutToServer=True, stderrToServer=True, suspend=False)

class VerticesGlobalPositionsTestCase(unittest.TestCase):
    def test_simple_global_positions(self) -> None:
        bpy.ops.wm.open_mainfile(filepath="Base.blend")
        hand = bpy.data.objects['Hand']
        mesh = MeshObject(hand)
        location = Vector([-100, -100, -100])
        hand.location = location
        self.assertEqual(hand.location, location)
        vertices_global_co = mesh.vertices_global_coordinates()
        vertex_global_co = vertices_global_co[0]
        self.assertAlmostEqual(vertex_global_co[0], -99.5137, delta=0.01)
        self.assertAlmostEqual(vertex_global_co[1], -99.9912, delta=0.01)
        self.assertAlmostEqual(vertex_global_co[2], -99.9668, delta=0.01)

    def test_offsets_between_two_global_locations(self) -> None:
        """
        Get the Base.blend file and training task description from:
        https://datagen.atlassian.net/wiki/spaces/IT/pages/252575772/Blender+Task
        """
        bpy.ops.wm.open_mainfile(filepath="Base.blend")
        hand = bpy.data.objects['Hand']
        mesh = MeshObject(hand)

        location1 = Vector([-100, -100, -100])
        hand.location = location1
        self.assertEqual(hand.location, location1)
        vs1 = mesh.vertices_global_coordinates()

        location2 = Vector([200, 200, 200])
        hand.location = location2
        self.assertEqual(hand.location, location2)
        vs2 = mesh.vertices_global_coordinates()

        self.assertEqual(vs1.keys(), vs2.keys())
        expected_offset = location1 - location2
        for vertex_index in vs1:
            actual_offset = vs1[vertex_index] - vs2[vertex_index]
            msg = "bad offset for vertex with index {}".format(vertex_index)
            self.assertEqual(expected_offset, actual_offset, msg)

if __name__ == "__main__":
    sys.argv = [__file__] + (sys.argv[sys.argv.index("--") + 1:] if "--" in sys.argv else [])
    unittest.main()

"""
Notes:
1. Run test from terminal and datagen-code/simulated-reality dir. 
clear && b --python <this file path> --background
"""
